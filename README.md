# Docker images for rav1e CI

This repository contains docker files for the rav1e images used for rav1e
CI builds. This repo itself has set up a CI Job to be able to build these
images automatically when they change (new commits), and upload them to the
Xiph Gitlab Docker registry autmatically.

To be able to do this, this project has a privileged runner, so be careful!
